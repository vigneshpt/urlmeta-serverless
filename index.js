'use strict';
const serverless = require('serverless-http');

var
	request = require('request').defaults({maxRedirects: 5,jar: true}),
	//setting jar: true to keep the cookies when redirecting.
	cheerio = require('cheerio'),
	http    = require('http'),
	uri     = require('url'),
	cluster = require('cluster'),
	valid   = require('validator'),
	Q 		= require('q');

var
	$        = null,	// to Hold cheerio DOM object
	response = null,
	metas    = null,
	resBack  = {},
	timeOut  = 7000,
	onlyHead = false,
	PORT = process.env.PORT || 9615,
  GA_TRACKING_ID = process.env.GA_TRACKING_ID;
  
function fullURL (url, path) {
	if(!path){
		path = "";
	}
  	return uri.resolve( url, path );
}

function encode_64 (url) {
	return new Buffer(url).toString('base64');
}

function trackEvent(category, type) {
  request.post(
    'http://www.google-analytics.com/collect', {
      form: {
		    v: '1',
		    tid: GA_TRACKING_ID,
		    cid: '555',
		    t: 'event',
		    ec: category,
		    ea: type === true ? 'onlyHead' : 'full'
		  }
  });
}

function getBody (url) {
	var q = Q.defer();
	request.get({
		url: url,
		timeout: timeOut,
		headers: {
			   'User-Agent': "URLMeta"
		}
	}, function (err, res, body) {

		if(!err && res.statusCode < 400) {

			resBack = {};
			try {
				$ = cheerio.load( cheerio( 'head', body ).html() );
				metas = $( 'meta[property], meta[name], meta[itemprop]' );
			} catch (e) {
				q.reject({ error: true, reason: 'Could not parse HTML of website.', code: 6  });
				return q.promise;
				//return respond({ error: true, reason: 'Could not parse HTML of website.', code: 6  });
			}

			if(metas !== null && metas.length > 0) {

				var meta = {};
				metas.each(function() {
					if( $( this ).attr('name') == 'urlmeta'  &&  $( this ).attr('content')  == 'no') {
						q.reject({ error: true, reason: 'Website does not allow crawling.', code: 3 });
						return q.promise;
						//respond({ error: true, reason: 'Website does not allow crawling.', code: 3 });
					} else if( $( this ).attr('property') == 'og:title' ||
										$( this ).attr('name') == 'twitter:title' ) {
						meta.title = $( this ).attr('content');
					} else if( $( this ).attr('property') == 'og:description' ||
										 $( this ).attr('name') == 'twitter:description' ||
										 $( this ).attr('itemprop') == 'description' ||
										 $( this ).attr('name') == 'description' ) {
						meta.description = $( this ).attr('content');
					} else if( $( this ).attr('property') == 'og:image' ||
										 $( this ).attr('name') == 'twitter:image' ||
										 $( this ).attr('itemprop') == 'image' ) {
						var value = $(this).attr('content') || $(this).attr('value');
						meta.image = fullURL( url, value);
					}
				});
				resBack.title       = meta.title;
				resBack.image       = meta.image;
				resBack.description = meta.description;

			}

			$('link[rel]').each(function() {
				if( $( this ).attr( 'rel' ) == 'shortcut icon' || $( this ).attr( 'rel' ) == 'icon' ) {
					resBack.favicon = fullURL( url, $( this ).attr( 'href' ) );
				}
			});

			var feed = $('link[rel=alternate]');
			if( feed.length > 0 ) {
				resBack.feed = {};
				resBack.feed.title = feed[0].attribs.title;
				resBack.feed.type = feed[0].attribs.type;
				resBack.feed.link = fullURL( url, feed[0].attribs.href );
			}

			if(!resBack.title) {
				resBack.title = $('title').text();
			}
			q.resolve(resBack);
			return q.promise;
			//return respond();

		} else if (err) {
			console.log(err);
			q.reject({ error: true, reason: err.message || 'Could not parse HTML of website.', code: 6 });
			return q.promise;
			//return respond({ error: true, reason: 'Could not parse HTML of website.', code: 6 });
		}

	});
	return q.promise;
}

function getHead (url){
	var q = Q.defer();
	request.head({
		url: url,
	    strictSSL: false,
		timeout: timeOut,
		headers: {
			   'User-Agent': "URLMeta"
		}
	}).on('response', function (response) {
	  if (response.statusCode == 200) {

	  	if(response.headers.urlmeta && response.headers.urlmeta == 'no') {
			q.reject({ error: true, reason: 'Website does not allow crawling.', code: 3 });
			return q.promise;
	  		//respond({ error: true, reason: 'Website does not allow crawling.', code: 3 });
			}

	  	var cont = response.headers['content-type'];
	  	resBack = {
	  			url: url,
	  			type: cont,
	  			size: response.headers['content-length']
	  		};
	  	if( cont.substr(0, 9) == 'text/html' && !onlyHead ) {
			  q.resolve();
			  return q.promise;
	  		//return getBody(url);
	  	} else {
			  onlyHead = true;
			  q.resolve(resBack);
			  return q.promise;
	  		//return respond();
	  	}

	  } else if (response.statusCode >= 400) {
		q.reject({ error: true, reason: 'Could not find what you were looking for.', httpCode: response.statusCode, code: 4 });
		return q.promise;
	  	//return respond({ error: true, reason: 'Could not find what you were looking for.', httpCode: response.statusCode, code: 4 });
	  }

	}).on('error', function(err) {
		q.reject({ error: true, reason: 'Request timed out. Website could not be reached in time.', code: 5 });
		return q.promise;
		//return respond({ error: true, reason: 'Request time out. Website could not be reached in time.', code: 5 });
	});
	return q.promise;
}

function respond (r) {
	r = r || {};

	var sendBack = {
		result: {}
	};

	if(r.error) {
		sendBack.result.status = 'ERROR';
		sendBack.result.code   = r.code;
		sendBack.result.reason =  r.reason || 'Unknown';

		if( sendBack.result.reason != 'Unknown' ) {
			sendBack.result.reason += ' Read docs here: https://urlmeta.org/dev-api.html';
		}

		if( r.httpCode ) {
			sendBack.result.httpCode = r.httpCode;
		}

	} else {
		sendBack.meta = {};
		sendBack.result.status = 'OK';

		if(r.type && r.type.substr(0, 9) == 'text/html')
			r.type = 'text/html';

		sendBack.meta = r;
	}

	if(onlyHead) {
		sendBack.result.onlyHead = true;
	}

	trackEvent('URL', onlyHead);
	response.writeHead(200, {'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*'});
	response.end( JSON.stringify( sendBack ) );
}

function startURL (url) {
	if( valid.isURL(url, { protocols: ['http','https'], require_protocol: true } ) ) {
		Q.allSettled([getHead(url), getBody(url)]).then(function(results){
			if(results[1].state == 'fulfilled' && results[1].value){
				respond(results[1].value);
			} else if(results[0].state == 'fulfilled' && results[0].value){
				respond(results[0].value);
			} else {
				var errorObj = results[0].reason || results[1].reason || {error: true, reason: "Could not process this URL", code: 2};
				respond(errorObj);
			}
		});
		//return getBody(url);
	} else if( url === undefined ) {
		respond( { error: true, reason: "Parameter URL not found.", code: 1 } );
	} else {
		respond( { error: true, reason: "Provided URL '"+ url +"' is not valid", code: 2 } );
	}
}

function init (req, res) {

	var query = uri.parse(req.url, true).query;

	onlyHead = (query.onlyHead !== undefined);
	response = res;
  return startURL( query.url );
	/* if(query.url && query.url.indexOf('urlmeta.soyou.co') > 0) {
		return respond({
			error: true,
			reason: "Somebody is getting cocky!"
		});
	} else {
		return startURL( query.url );
	} */
}

function test(req, res){
  res.writeHead(200, {'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*'});
  res.end(JSON.stringify({message:"Hello world from API"}));
}

module.exports.handler = serverless(init);

/* module.exports = (event, context, callback) => {
  const response = {
    statusCode: 200,
    body: JSON.stringify({
      message: 'Go Serverless v1.0! Your function executed successfully!',
      input: event,
    }),
  };

  callback(null, response);

  // Use this code if you don't use the http event with the LAMBDA-PROXY integration
  // callback(null, { message: 'Go Serverless v1.0! Your function executed successfully!', event });
}; */
