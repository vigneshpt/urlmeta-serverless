**Deployment with local machine**:
1. Install serverless with `npm i -g serverless`
2. Make sure that the files `~/.aws/config` and `~/.aws/credentials` have been configured. 
3. If not run `aws configure --profile eb-cli`
4. To deploy, run `AWS_PROFILE=eb-cli serverless deploy -v`
